# coding: utf-8
Encoding.default_external = 'UTF-8'
require 'sinatra'
require 'sinatra/reloader'
require 'sinatra/static_assets'
require 'sinatra/cross_origin'
require 'json'
require 'rubyXL'
require 'mail'
require_relative 'models/User'
require_relative 'models/BBS_Thread'
require_relative 'models/BBS'
require_relative 'models/Match'
require_relative 'models/Team'
require_relative 'models/Order'
require_relative 'models/Performance'

enable :sessions

helpers do
  include Rack::Utils
  alias_method :h, :escape_html
end

#brタグ変換
def convertBr(str)
  str.gsub(/\r\n|\r|\n/,'<br />')
end
#ユーザーの画像表示
def profileImg(path)
  image_tag("img/#{path}")
end

#ユーザー名の取得
def selectName(userid)
  selectuser = User.where(User_id: userid).first
  return selectuser.UserName
end

def printadmin(flag)
  if flag == "1" then
    return "管理者"
  end
end

def space_check(space)
  p space
  if space == "○" then
    return "出席"
  else
    return "欠席"
  end
end

before do
  if session[:userinfo]
    @user = User.where(Password_hash: session[:userinfo]).first
  end
end

get '/mypage' do
  if @user
    @admin_user = User.admin_check(@user.User_id)
    if @admin_user == "×"
      erb :mypage_popular
    else
      erb :mypage
    end
  else
    redirect '/login'
  end
end

get '/signup' do
  erb :signup, :layout => false
end

post '/signup' do
  if params[:password] != params[:password_conf]
    redirect '/signup'
  end
  
  if User.signup(params[:username], params[:address], params[:password])
    redirect '/login'
  else
    redirect '/signup'
  end
end

get '/login' do
  if session[:userinfo]
    redirect '/mypage'
  end
  erb :login, :layout => false
end

post '/login' do
  if session[:userinfo]
    redirect '/mypage'
  end
    
  user = User.auth(params[:address], params[:password])
  if user
    session[:userinfo] = user.Password_hash
    redirect '/mypage'
  else
    redirect '/login'
  end
end

delete '/logout' do
  session[:userinfo] = nil
  redirect '/login'
end

get '/thread' do
  if session[:userinfo].blank?
    redirect '/login'
  end
  @threads = BBS_Thread.threadList
  erb :thread
end

post '/thread_create' do
  BBS_Thread.create(params[:threadTitle], @user.User_id)
  redirect '/thread'
end

get '/bbs/:threadtitle' do
  if session[:userinfo].blank?
    redirect '/login'
  end
  @thread = BBS_Thread.find_by(ThreadTitle: params[:threadtitle])
  @comments = BBS.search_all(@thread.Thread_id)
  erb :bbs
end

post '/posting' do
  BBS.insert(params[:threadid],params[:userid],params[:comment])
  redirect '/bbs/' + params[:title]
end

get '/profile' do
  if @user
    erb :profile
  else
    redirect '/login'
  end
end

post '/profile' do
  User.profileUpdate(params[:userid], params[:username], params[:introduction])
  redirect '/profile'
end

post '/upload_img' do
  if params[:file]
    filename = "#{@user.UserName}_#{params[:file][:filename]}"
    save_path = "public/img/#{filename}"
      File.open(save_path, 'wb') do |f|
        f.write params[:file][:tempfile].read
      end
    User.setImage(@user.User_id, filename)
    redirect '/profile'
  end
end

#試合予定一覧ページ
get '/match' do
  if session[:userinfo].blank?
    redirect '/login'
  end
  
  @user = User.my_name(session[:userinfo])
  @matches = Match.all_match
  @admin_user = User.admin_check(@user.User_id)
  if @admin_user == "×"
    erb :match_popular
  else
    erb :match
  end
end

#過去の試合ページ
get '/past_match' do
  if session[:userinfo].blank?
    redirect '/login'
  end
  @end_matches = Match.end_match
  erb :past_match
end

#試合予定追加
post '/add_match' do
  Match.add_match(params['matchDate'],params['enemyTeam'],params['groundName'])
  redirect '/match'
end

delete '/match_operation' do
  user = User.my_name(session[:userinfo])
  if params['flag']
    Match.match_update(params['enemyTeam'],params['groundName'],params['match_id'],params['matchDate'])
    redirect '/match'
  end
  if params['attend_flag'] == 'attend'
    Order.attend_match(params['match_id'],user['User_id'])
    redirect '/match'
  end
  if params['attend_flag'] == 'absence'
    Order.absence_match(params['match_id'],user['User_id'])
    redirect '/match'
  end
  if params['score_button']
    $match = Match.where(Match_id: params['match_id']).first
    redirect '/score'
  end
  if params['order_button']
    $order = params['match_id']
    redirect '/order'
  end
  Match.delete_match(params['match_id'])
  redirect '/match'
end

#メンバー一覧
get '/member_all' do
  @member = User.all_member
  erb :member_all
end

#チームページ
get '/teampage' do
  @Team = Team.all_contents
  erb :teampage
end

#編集ページ
get '/editpage' do
  if session[:userinfo].blank?
    redirect '/login'
  end
  @Team = Team.all_contents
  erb :teampage_edit
end

#チーム編集後
post '/editTeam' do
  Team.update_contents(params['place'],params['introduction'])
  redirect '/teampage'
end

get '/score' do
  if session[:userinfo].blank?
    redirect '/login'
  end
  @match = $match
  erb :score
end

post '/score' do
  teampoint = params[:teampoint].map(&:to_i).inject(:+)
  enemypoint = params[:enemypoint].map(&:to_i).inject(:+)
  if teampoint > enemypoint then
    winflag = "勝"
  elsif teampoint == enemypoint then
    winflag = "引"
  else
    winflag = "負"
  end
  Match.add_score(params['matchid'],teampoint,enemypoint,winflag)
  redirect '/past_match'
end

get '/scoreall' do
  @matches = Match.allscore
  erb :scoreall
end

get '/order' do
  if session[:userinfo].blank?
    redirect '/login'
  end
  @order = $order
  @player = Order.attend_player(@order)
  @order_decision = Order.decision_player(@order)
  erb :order
end

post '/order' do
  @order = $order
 arr = JSON.parse(request.body.read)
  arr.each do |hash|
        @batting = hash['打順']
        @OrderName = hash['名前']
        @OrderPosition = hash['守備']
      user = User.select_user(@OrderName)
      Order.order_update(@order,user,@OrderPosition,@batting)
  end
  end

get '/seiseki' do
  if session[:userinfo].blank?
    redirect '/login'
  end
  @date = params['date']
  @performances = Performance.allperformance
  erb :seiseki
end

post '/seiseki' do
  if params[:file]
    save_path = "public/excel/#{params[:file][:filename]}"
      File.open(save_path, 'wb') do |f|
        f.write params[:file][:tempfile].read
      end
    workbook = RubyXL::Parser.parse("#{save_path}")
    worksheet = workbook[0]
    headers = worksheet.extract_data[0]
    seiseki = worksheet.get_table(headers)[:table]    
    seiseki.each do |user|
      p_user = User.where(UserName: user['名前']).first
      Performance.addPerformance(params['date'],p_user.User_id,user['打席'],user['打数'],user['安打'],user['1打'],user['2打'],user['3打'],user['本塁打'],user['打点'],user['得点'],user['三振'],user['四球'],user['死球'],user['犠打'],user['犠飛'],user['出塁'],user['盗塁'],user['失策'],user['フィーバー'],
      user['登板'],user['勝利'],user['敗北'],user['救援'],user['投球回'],user['対打者'],user['被安打'],user['被本塁打'],user['奪三振'],user['与四球'],user['与死球'],user['牽制'],user['失点'],user['自責点'],user['強制交代'])
    end
    redirect '/performance'
  end
end

get '/performance' do
  if session[:userinfo].blank?
    redirect '/login'
  end
  @date = params['date']
  @performances = Performance.allperformance
  erb :performance
end

get '/admin' do
  if session[:userinfo].blank?
    redirect '/login'
  end
  @checkuser = User.my_name(session[:userinfo])
  @admin = User.admin_check(@checkuser.User_id)
  @members = User.all_member
  if @admin == "×"
    erb :mypage
  else
    erb :admin
  end
end

post '/backnumber' do
  backnumbers = params['backnumber']
  member = User.all_member
  p params['backnumber']
  p backnumbers['rin']
    member.each do |user|
    if backnumbers[user.UserName].present?
      User.updateBacknumber(user.User_id, backnumbers[user.UserName])
    end
  end
  redirect '/admin'
end

post '/invite' do
mail = Mail.new

options = { :address              => "smtp.gmail.com",
            :port                 => 587,
            :domain               => "smtp.gmail.com",
            :user_name            => 'baseballoic@gmail.com',
            :password             => 'baseballoic6772',
            :authentication       => :plain,
            :enable_starttls_auto => true  }        
mail.charset = 'utf-8'
mail.from "baseballoic@gmail.com"
mail.to "#{params['address']}"    
mail.subject "招待メール"
mail.body "やきゅうしようよ\nhttp://baseball.utopie.jp/signup"
mail.delivery_method(:smtp, options)
mail.deliver
redirect '/admin'
end

post '/authority' do
  params["adminlist"].each do |user|
    admin = User.where(UserName: user).first
    User.authority(admin.User_id)
  end
  redirect '/admin'
end

get '/:username' do
  @userinfo = User.where(UserName: params[:username]).first
  if @userinfo
    @score = Match.allscore
    erb :userpage
  end
end