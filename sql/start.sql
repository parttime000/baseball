drop database if exists baseball;
create database baseball;
create table baseball.users(
  User_id int(100) NOT NULL PRIMARY KEY auto_increment,
	UserName varchar(10),
	Admin_Flag varchar(10),
	MailAddress varchar(50) UNIQUE,
	Password_salt varchar(255),
	Password_hash varchar(255),
	Birthday date,
	BackNumber varchar(3) UNIQUE,
	Icon varchar(100) default 'default.jpg',
	Profile varchar(400)) engine = InnoDB;

create table baseball.bbs_threads(
  Thread_id int (100) not null primary key auto_increment,
	User_id int(100) not null,
	ThreadTitle varchar(30),
	DateModified datetime) engine = InnoDB;

create table baseball.bbs(
	BBS_id int(100) not null primary key auto_increment,
	Thread_id int(100) not null,
	User_id int(100) not null,
	Contents text(1000),
	PostDate datetime)engine = InnoDB;

create table baseball.matches(
  Match_id int(100) not null primary key auto_increment,
	MatchDate date,
	EnemyTeam varchar(100),
	GroundName varchar(100),
	End_Flag varchar(10) DEFAULT 'NotGame',
	Win_Flag varchar(3),
	Team_Point int(2),
	Enemy_Point int(2))engine = InnoDB;

create table baseball.teams(
	Team_id int(100) NOT NULL  PRIMARY KEY auto_increment,
	TeamName varchar(50),
	Active_Place varchar(50),
	Team_Introduction varchar(1000));

create table baseball.orders(
	Match_id int(100) not null PRIMARY  KEY,
	User_id int(100) not null,
	attend_flag varchar(1),
	Position varchar(10),
	Batting_order int(2))engine = InnoDB;

create table baseball.performances(
	User_id int(100) not null PRIMARY KEY,
	UpLoad_date date,
	AT_Bat int(2),AT_Bats int(2),
	Hit int(2),Single_hit int(2),
	Twobase_hit int(2),
	Threebase_hit int(2),
	HomeRun int(2),
	RBI int(2),
	Score int(3),
	Struckout int(2),
	Walk int(2),
	Deadball int(2),
	Sacrifice int(2),
	Sacrifice_Fly int(2),
	On_base int(2),
	Stolen_base int(2),
	Blunder int(2),
	Fever int(2),
	Mound int(3),
	Victory int(2),
	Defeat int(2),
	Relief int(2),
	Pitching_times int(2),
	Versus_batter int(3),
	The_Hits int(2),
	Home_runs_allowed int(2),
	Strikeouts int(2),
	Walks_given int(2),
	Deadball_given int(2),
	Pick_off int(2),
	Runs_allowed int(2),
	Earned_runs int(2),
	Force_change int(2))engine = InnoDB;

alter table baseball.orders drop primary key;
alter table baseball.orders add constraint primary key(Match_id,User_id);

alter table baseball.performances DROP  PRIMARY  KEY ;
alter table baseball.performances add constraint PRIMARY  KEY (User_id,UpLoad_date);

alter table baseball.bbs_threads add foreign key(User_id) references baseball.users(User_id) on UPDATE CASCADE on DELETE CASCADE ;

alter table baseball.bbs add foreign key(Thread_id) references baseball.bbs_threads(Thread_id) on UPDATE CASCADE on DELETE CASCADE;
alter table baseball.bbs add foreign key(User_id) references baseball.users(User_id) on UPDATE CASCADE on DELETE CASCADE;

alter table baseball.orders add foreign key(Match_id) references baseball.matches(Match_id) on UPDATE CASCADE on DELETE CASCADE;
alter table baseball.orders add foreign key(User_id) references baseball.users(User_id) on UPDATE CASCADE on DELETE CASCADE;

alter table baseball.performances add FOREIGN  KEY (User_id) REFERENCES  baseball.users(User_id) on UPDATE CASCADE on DELETE CASCADE;


insert into baseball.users(UserName, Admin_Flag, MailAddress, Password_salt, Password_hash) values("base", 1, "baseballoic@gmail.com", "2518cde4f41f247ca9b5fdfb56e13c574eac4ea2","c52eeb72aacb26bc98eeaef53115d87f3b83a1ca");