require 'active_record'
require 'mysql2'
require 'digest/sha1'

ActiveRecord::Base.configurations = YAML.load_file('database.yml')
ActiveRecord::Base.establish_connection(:development)

class Team < ActiveRecord::Base

  def self.update_contents(place,introduction)
    team = Team.find(1)
    team.Active_Place = place
    team.Team_Introduction = introduction
    team.save
  end

  def self.all_contents
    contents = self.find(1)
    return contents
  end
end