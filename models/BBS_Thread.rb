require 'active_record'
require 'mysql2'
require 'date'

ActiveRecord::Base.configurations = YAML.load_file('database.yml')
ActiveRecord::Base.establish_connection(:development)

class BBS_Thread < ActiveRecord::Base
  #スレッド作成
  def self.create(title, userid)
    thread = BBS_Thread.new
    thread.User_id = userid
    thread.ThreadTitle = title;
    thread.DateModified = Time.now
    thread.save
  end
  
  #スレッド一覧取得
  def self.threadList
    threads = BBS_Thread.all
    return threads
  end
end