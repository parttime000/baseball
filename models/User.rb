require 'active_record'
require 'mysql2'
require 'digest/sha1'

ActiveRecord::Base.configurations = YAML.load_file('database.yml')
ActiveRecord::Base.establish_connection(:development)

class User < ActiveRecord::Base
  attr_readonly :Password_salt, :Password_hash
  validates_presence_of :UserName
  validates_presence_of :MailAddress
  validates_uniqueness_of :MailAddress
  validates_format_of :MailAddress, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  validates_presence_of :Password_salt
  validates_presence_of :Password_hash
  
  #ユーザー作成
  def self.signup(username, address, password)
    user = User.new
    user.UserName = username
    user.MailAddress = address
    user.Password_salt = self.salt(address)
    user.Password_hash = self.hash(password, user.Password_salt)
    user.save
  end
  
  #ユーザー認証
  def self.auth(address, password)
    user = self.where(MailAddress: address).first
    if user && user.Password_hash == self.hash(password, user.Password_salt)
      user
    else
      nil
    end
  end
  
  #ユーザー情報更新
  def self.profileUpdate(userid, username, profile)
    user = self.where(User_id: userid).first
    user.UserName = username
    user.Profile = profile
    user.save
  end
  
  #ユーザーの画像設定
  def self.setImage(userid, path)
    user = self.where(User_id: userid).first
    user.Icon = path
    user.save
  end
  
  #salt生成
  def self.salt(address)
    Digest::SHA1.hexdigest("#{Time.now.to_s}#{address}")
  end

  #ハッシュ化
  def self.hash(password, salt)
    Digest::SHA1.hexdigest("#{salt}#{password}")
  end
  
  #メンバー一覧取得
  def self.all_member
    member = self.all
    return member
  end
  
  #ハッシュ値からユーザー情報を取得
  def self.my_name(hash)
    user = self.where(Password_hash: hash).first
    return user
  end
  
  def self.admin_check(user)
     self.where(Admin_Flag: 1).each do |admin_user|
        if user == admin_user['User_id']
          return "○"
        end
     end
    return "×"
  end
  
  #背番号更新
  def self.updateBacknumber(userid, backnumber)
    user = self.where(User_id: userid).first
    user.BackNumber = backnumber
    user.save
  end
  
  #権限変更
  def self.authority(userid)
    user = self.where(User_id: userid).first
    if user.Admin_Flag == "1" then
      user.Admin_Flag = ""
    else
      user.Admin_Flag = 1
    end
    user.save
  end

  def self.select_user(user_name)
     User.find_by_sql("select user_id from users where username= '#{user_name}'").each do |user|
      return user['user_id']
    end
  end
end
