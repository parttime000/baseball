require 'active_record'
require 'mysql2'
require 'digest/sha1'

ActiveRecord::Base.configurations = YAML.load_file('database.yml')
ActiveRecord::Base.establish_connection(:development)

class Match < ActiveRecord::Base

  #試合情報の新規作成
  def self.add_match(matchDate,enemyTeam,groundName)
    match = Match.new
    match.MatchDate = matchDate
    match.EnemyTeam = enemyTeam
    match.GroundName = groundName
    match.save
  end

  #終了した試合情報の取得
  def self.end_match
    end_matches = self.find_by_sql("select * from matches where End_Flag = 'End' order by MatchDate desc")
    return end_matches
  end

#終了していない試合情報の取得
  def self.all_match
    matches = self.find_by_sql("select * from matches where End_Flag = 'NotGame'")
    return matches
  end

  def self.delete_match(delete_id)
    self.find_by(match_id: delete_id).destroy
  end

#試合情報の更新
  def self.match_update(team,ground,id,day)
    match = Match.where(Match_id: id).first
    match.EnemyTeam = team
    match.GroundName = ground
    match.MatchDate = day
    match.save
  end
    
  def self.add_score(matchid, teampoint, enemypoint, winflag)
    match = Match.where(Match_id: matchid).first
    match.Team_Point = teampoint
    match.Enemy_Point = enemypoint
    match.End_Flag = "End"
    match.Win_Flag = winflag
    match.save
  end
  
  def self.allscore
    matches = Match.where(End_Flag: "End").order(MatchDate: :asc)
    return matches
  end
end

class Order < ActiveRecord::Base

  #出席処理
  def self.attend_match(match_id,user_id)
    orders = Order.new()
    orders.Match_id = match_id
    orders.User_id = user_id
    orders.attend_flag = "○"
    orders.save
  end

  #出席解除処理
  def self.absence_match(match_id,user_id)
    Order.delete_all(Match_id: match_id,User_id: user_id)
  end
  
  def self.attend_check(user_id,match_id)
      Order.where(User_id: user_id,Match_id: match_id,attend_flag: "○").each do |order_contents|
          return order_contents['attend_flag']
     end
  end
end
