require 'active_record'
require 'mysql2'
require 'composite_primary_keys'

ActiveRecord::Base.configurations = YAML.load_file('database.yml')
ActiveRecord::Base.establish_connection(:development)

class Order < ActiveRecord::Base
  self.primary_keys = :Match_id,:User_id
  def self.insert(matchid, userid, position)
    order = Order.new
    order.Match_id = matchid
    order.User_id  = userid
    order.Position = position
    order.save
  end

  def self.attend_player(matchid)
    attend_player = User.find_by_sql("select * from orders inner join
                                  users on(orders.user_id = users.user_id)
                                  where orders.match_id = '#{matchid}'")
    return attend_player
  end

  def self.decision_player(match_id)
    decision_player = User.find_by_sql("select * from orders inner join
                                  users on(orders.user_id = users.user_id)
                                  where orders.match_id = '#{match_id}' order by batting_order asc")
  end

  def self.order_update(match_id,user_id,position,batting)
   order = self.where(Match_id:match_id,User_id: user_id).first
   order.Position = position
   order.Batting_order = batting
   order.save
  end

end