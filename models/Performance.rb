require 'active_record'
require 'mysql2'
require 'digest/sha1'
require 'date'

ActiveRecord::Base.configurations = YAML.load_file('database.yml')
ActiveRecord::Base.establish_connection(:development)

class Performance < ActiveRecord::Base
  
  #成績データ追加
  def self.addPerformance(date,userid,at_bat,at_bats,hit,single,twobase,threebase,homerun,rbi,score,struckout,walk,deadball,sacrifice,sacrifice_fly,on_base,stolen_base,blunder,fever,
  mound,victory,defeat,relief,pitching_times,vs_batter,the_hits,homerun_allowed,strikeouts,walks_given,deadball_given,pick_off,runs_allowed,earned_runs,force)
    performance = Performance.new
    performance.UpLoad_date = Date.strptime(date, "%Y-%m-%d")
    performance.User_id = userid
    performance.AT_Bat = at_bat
    performance.AT_Bats = at_bats
    performance.Hit = hit
    performance.Single_hit = single
    performance.Twobase_hit = twobase
    performance.Threebase_hit = threebase
    performance.HomeRun = homerun
    performance.RBI = rbi
    performance.Score = score
    performance.Struckout = struckout
    performance.Walk = walk
    performance.Deadball = deadball
    performance.Sacrifice = sacrifice
    performance.Sacrifice_Fly = sacrifice_fly
    performance.On_base = on_base
    performance.Stolen_base = stolen_base
    performance.Blunder = blunder
    performance.Fever = fever
    performance.Mound = mound
    performance.Victory = victory
    performance.Defeat = defeat
    performance.Relief = relief
    performance.Pitching_times = pitching_times
    performance.Versus_batter = vs_batter
    performance.The_Hits = the_hits
    performance.Home_runs_allowed = homerun_allowed
    performance.Strikeouts = strikeouts
    performance.Walks_given = walks_given
    performance.Deadball_given = deadball_given
    performance.Pick_off = pick_off
    performance.Runs_allowed = runs_allowed
    performance.Earned_runs = earned_runs
    performance.Force_change = force
    performance.save
  end
  
  #ユーザー毎の全ての成績
  def self.allperformance
    performances = self.find_by_sql("select User_id, sum(AT_Bat) as AT_Bat,sum(AT_Bats) as AT_Bats,sum(Hit) as Hit,sum(Single_hit) as Single_hit,sum(Twobase_hit) as Twobase_hit,sum(Threebase_hit) as Threebase_hit,
                                                     sum(HomeRun) as HomeRun,sum(RBI) as RBI,sum(Score) as Score,sum(Struckout) as Struckout,sum(Walk) as Walk,sum(Deadball) as Deadball,sum(Sacrifice) as Sacrifice,
                                                     sum(Sacrifice_Fly) as Sacrifice_Fly,sum(On_base) as On_base,sum(Stolen_base) as Stolen_base,sum(Blunder) as Blunder,sum(Fever) as Fever,sum(Mound) as Mound,
                                                     sum(Victory) as Victory,sum(Defeat) as Defeat,sum(Relief) as Relief,sum(Pitching_times) as Pitching_times,sum(Versus_batter) as Versus_batter,
                                                     sum(The_Hits) as The_Hits,sum(Home_runs_allowed) as Home_runs_allowed,sum(Strikeouts) as Strikeouts,sum(Walks_given) as Walks_given,sum(Deadball_given) as Deadball_given,
                                                     sum(Pick_off) as Pick_off,sum(Runs_allowed) as Runs_allowed,sum(Earned_runs) as Earned_runs,sum(Force_change) as Force_change from performances group by User_id")
    return performances
  end
end