require 'active_record'
require 'mysql2'
require 'date'

ActiveRecord::Base.configurations = YAML.load_file('database.yml')
ActiveRecord::Base.establish_connection(:development)

class BBS < ActiveRecord::Base
  #スレッドへコメント投稿
  def self.insert(threadid, userid, comment)
    bbs = BBS.new
    bbs.Thread_id = threadid
    bbs.User_id = userid
    bbs.Contents = comment
    bbs.PostDate = Time.now.strftime('%Y-%m-%d %H:%M:%S')
    bbs.save
  end
  
  #指定したスレッド内のコメント一覧
  def self.search_all(threadid)
    comments = BBS.where(Thread_id: threadid).order(PostDate: :desc)
    return comments
  end
end