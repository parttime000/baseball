$(function(){
    $("#day").datepicker();
});

function edit_match(match_id,teamName,groundName,matchDate){

    var team =  document.getElementById("team"+match_id);
    var ground = document.getElementById("ground"+match_id);
    var date = document.getElementById("date"+match_id);

    team.innerHTML = "<input type='text' name='enemyTeam' value= "+teamName+">";
    ground.innerHTML = "<input type='text' name='groundName' value="+groundName+">";
    date.innerHTML = "<input type='text' name='matchDate' id='DatePicker' value="+matchDate+">";
    $(function(){
        $("#DatePicker").datepicker();
    });
    document.getElementById("score"+match_id).disabled = true;
    document.getElementById("order"+match_id).disabled = true;
    document.getElementById("delete_button"+match_id).disabled = true;
    document.getElementById("edit_button"+match_id).disabled = true;
    document.getElementById("attendbutton"+match_id).disabled  = true;
    document.getElementById("submitbutton"+match_id).innerHTML = "<input type='submit'  class='btn btn-default btn-ghost' value='更新'>";
    document.getElementById("flag"+match_id).innerHTML = "<input type='hidden' name='flag' value='edit'>";

}
