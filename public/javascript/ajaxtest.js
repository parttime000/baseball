$(function(){
  $('#btn').click(function () {
    var order_number = $('.order_number .orderTable').tableToJSON();
    var player_name = $('.player_name .orderTable').tableToJSON();
    var position = $('.position .orderTable').tableToJSON();
    $.extend(true, order_number, player_name, position)
    $.ajax({
      data: JSON.stringify(order_number),
      type: 'post',
      url: '/order',
      datatype: 'json',
      contentType: 'application/json',
      success: function(){ location.reload(true); }
    });
  });
});